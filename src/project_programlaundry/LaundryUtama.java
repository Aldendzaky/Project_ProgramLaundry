/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project_programlaundry;

/**
 *
 * @author Alden Dzaky A
 */
import java.util.Scanner;
public class LaundryUtama {
    static int pilih;
    static int menu;
    static Scanner masuk = new Scanner(System.in);
    static public void menu() {
        System.out.println("===Selamat Datang===");
        System.out.println("Apa jabatan anda?");
        System.out.println("1. Pelanggan");
        System.out.println("2. Staff");
        System.out.println("3. Administrator");
        System.out.println("4. Owner");
        System.out.println("5. Keluar");
        System.out.print("Jawab : ");
        pilih = masuk.nextInt();
    }
    public static void main(String[] args) {
        menu();
        switch(pilih){
            case 1 :
                LaundryPelanggan pelanggan = new LaundryPelanggan();
                pelanggan.masuk();
                System.out.println("Kembali ke menu?");
                System.out.println("1. Ya");
                System.out.println("2. Tidak");
                System.out.print("Jawab : ");
                menu = masuk.nextInt();
                if(menu == 1){
                    menu();
                }else
                    System.exit(0);
            case 2 :
                LaundryStaff staff = new LaundryStaff();
                staff.proses1();
                System.out.println("Kembali ke menu?");
                System.out.println("1. Ya");
                System.out.println("2. Tidak");
                System.out.print("Jawab : ");
                menu = masuk.nextInt();
                if(menu == 1){
                    menu();
                }else
                    System.exit(0);
            case 3 :     
                laundryAdmin admin = new laundryAdmin();
                admin.proses2();
                System.out.println("Kembali ke menu?");
                System.out.println("1. Ya");
                System.out.println("2. Tidak");
                System.out.print("Jawab : ");
                menu = masuk.nextInt();
                if(menu == 1){
                    menu();
                }else
                    System.exit(0);
            case 4 : 
                LaundryOwner pemilik = new LaundryOwner();
                pemilik.report();
                System.out.println("Kembali ke menu?");
                System.out.println("1. Ya");
                System.out.println("2. Tidak");
                System.out.print("Jawab : ");
                menu = masuk.nextInt();
                if(menu == 1){
                    menu();
                }else
                    System.exit(0);
            case 5 : 
                System.exit(0);
            default : 
                System.out.println("Pilihan yang anda masukkan salah");
                menu();
        }
        
    }
    
}


